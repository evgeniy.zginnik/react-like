import { BaseComponent } from '../';

class List extends BaseComponent {

  static propTypes = {
    data: 'Array<string>',
    loadListOfFiles: 'function'
  };

  constructor(...props) {
    super(...props);
    console.log(0, 'constructor')
  }

  componentWillMount() {
    console.log(1, 'componentWillMount');
  }

  componentDidMount() {
    this.props.loadListOfFiles();
    console.log(2, 'componentDidMount');
  }

  render() {
    const html = this.props.data.map(item => `<li>${item}</li>`)
    this.node.innerHTML = `<ul>${html}</ul>`
  }
}

const body = document.body;

const list = new List({ data: ['AAAAA'], loadListOfFiles }, body); // render AAAAA

function loadListOfFiles() {
  setTimeout(() => this.data = ['AAAAAA', 'BBBBB'], 2000); // render 'AAAAAA', 'BBBBB'
}

list.data = []; // render empty array (remove all content from the page)